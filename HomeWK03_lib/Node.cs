﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWK03_lib
{
    class Node<ListType>
    {
        public ListType value;
        public Node<ListType> next;
        public Node(ListType value)
        {
            this.value = value;
            next = null;
        }
    }
}
