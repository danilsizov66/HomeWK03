﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWK03_lib
{
    public class MineList<ListType>
    {
        internal Node<ListType> Head;
        public int Size = 0;
        public void Add(ListType value)
        {
            ++Size;
            Node<ListType> new_INode = new Node<ListType>(value);
            if (this.Head == null)
            {
                this.Head = new_INode;
                return;
            }
            Node<ListType> lastNode = GetLastNode();
            lastNode.next = new_INode;
        }
        internal Node<ListType> GetLastNode()
        {
            Node<ListType> temp = this.Head;
            while (temp.next != null)
            {
                temp = temp.next;
            }
            return temp;
        }

        public void RemoveAt(int index)
        {
            --Size;
            Node<ListType> temp = this.Head;
            Node<ListType> prev = null;
            
            if (index > Size)
            {
                return;
            }

            for (int i = 0; i < index; i++)
            {
                if (temp == null)
                {
                    return;
                }
                prev = temp;
                temp = temp.next;
            }   
            prev.next = temp.next;
        }
        public string PrintAll()
        {
            string str = " ";
            if (this.Head == null)
            {
                str = "List is empty";
                return str; ;
            }

            Node<ListType> temp = this.Head;
            while (temp != null)
            {
                str += Convert.ToString(temp.value) + " ";  
                temp = temp.next;
            }
            return str;
        }
        public MineList()
        {

        }
    }
}
